<?php
/*
	Template Name: Landningssida
 */

// ACF
$sliderbg		= get_field('sliderbg');
$imgover		= get_field('imgover');
$slidertxt		= get_field('slidertxt');
$imgunder		= get_field('imgunder');

$event_loop_img			= get_field('event_loop_img');
$event_loop_head		= get_field('event_loop_head');
$event_loop_text		= get_field('event_loop_text');
$event_loop_linktext	= get_field('event_loop_linktext');
$event_loop_link		= get_field('event_loop_link');

$eventdate		= get_field('eventdate');
$eventstart		= get_field('eventstart');

$parallax_img	= get_field('parallax_img');
$parallax_text	= get_field('parallax_text');

$christmas_public = get_field('christmas_public');
$christmas_img = get_field('christmas_img');
$christmas_presents = get_field('christmas_presents');
$christmas_title = get_field('christmas_title');
$christmas_subtitle = get_field('christmas_subtitle');
$christmas_txt = get_field('christmas_txt');
$christmas_btn_txt = get_field('christmas_btn_txt');
$christmas_btn_link = get_field('christmas_btn_link');

get_header(); ?>

<!-- HERO
    ================================================== -->
    <section id="slider">
		<?php putRevSlider("valgrund") ?>
    </section>



    <!-- DETTA HÄNDER PÅ VALAND
	================================================== -->
	<section id="event-loop">
		<div class="container">
			<div class="row">

				<div class="col-sm-4">
				<div class="event">
					<div class="event-content-no-col first">
				<?php if( !empty($event_loop_img) ) : ?>
    				<img src="<?php echo $event_loop_img['url']; ?>" alt="<?php echo $event_loop_img['alt']; ?>">
		    	<?php endif; ?>
					<h2><?php echo $event_loop_head; ?></h2>
					<p class="lead"><?php echo $event_loop_text; ?></p>
					<p><a href="<?php echo $event_loop_link; ?>"><?php echo $event_loop_linktext; ?></a></p>
					</div><!-- event-content -->
				</div><!-- event -->
				</div>

				<?php
					$today = date('Ymd');

					$args = array (
    					'post_type' 	=> 'valevent',
    					'meta_key'		=> 'eventdate',
    					'orderby' 		=> 'meta_value',
						'order' 		=> 'ASC',
						'posts_per_page' => 5,
    					'meta_query' 	=> array(
	        				'key'		=> 'eventdate',
	        				'compare'	=> '>=',
	        				'value'		=> $today,
						),
					);

// get posts
$loop = new WP_Query( $args );
?>
				<?php while( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="col-sm-4">
					<div class="event event-hover" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')">
					<a href="<?php the_permalink() ?>" class="event-content">
					<h4><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></h4>
					<h3><?php the_title(); ?></h3>
          <h4>
            <?php if( get_field('eventstart') ): ?><?php the_field('eventstart'); ?>
            <?php endif; ?>
            <?php the_field('eventdate'); ?>
          </h4>
					</a><!-- event-content -->
					</div><!-- event -->
				</div>

				<?php endwhile; ?>

			</div><!-- event row -->
		</div><!-- container -->
	</section><!-- event loop-->


	<!-- PARALLAX
	================================================== -->
	<section id="featurette" style="background-image: url('<?php echo $parallax_img; ?>')">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<h1>
					<?php if( !empty($parallax_text) ) : ?>
						<?php echo $parallax_text; ?>
					<?php endif; ?>
						</h1>
				</div><!-- end col -->
			</div><!-- row -->
		</div><!-- container -->
	</section><!-- featurette -->

<?php
get_footer();
