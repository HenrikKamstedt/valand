<aside id="sidebar-open" class="col-sm-3">	
	<div class="widget">
		<h3 class="gold">Bokning</h3>
		<p class="disclaimer">E-post: <a href="mailto:bokning@valand.se">bokning@valand.se</a></p>
	</div>
	
	<div class="widget">
		<h3 class="gold">Lilla London</h3>
		<p class="disclaimer">Telefon: 031-18 40 62</p>
	</div>
	
	<div class="widget">
		<h3 class="gold">Nattklubb</h3>
		<p class="disclaimer">Telefon: 031-18 30 93</p>
	</div>		
	
	<div class="widget">
		<h3 class="gold">Festvåning</h3>
		<p class="disclaimer">Telefon: 031-18 30 94</p>
	</div>
	
	<div class="widget">
		<h3 class="gold">Borttappat/Upphittat</h3>
		<p class="disclaimer">E-post: <a href="mailto:info@valand.se">info@valand.se</a></p>
	</div>
	
		<div class="widget">
		<h3 class="gold">Live/Evenemang</h3>
		<p class="disclaimer">E-post: <a href="mailto:magnus@valandlive.se">magnus@valandlive.se</a><br>
			E-post: <a href="mailto:christian@valandlive.se">christian@valandlive.se</a>
			</p>	
	</div>

	<div class="widget">
		<h3 class="gold">Ekonomiavdelning</h3>
		<p class="disclaimer">E-post: <a href="mailto:ekonomi@valand.se">ekonomi@valand.se</a></p>	
	</div>
</aside><!-- sidbar open -->