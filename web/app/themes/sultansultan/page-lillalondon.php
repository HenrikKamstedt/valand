<?php
/**
	Template Name: Lilla London
 */

// ACF
$post_id 		= 283;
$open			= get_field('open', 283);
$gaster			= get_field('gaster', 283);
$booking		= get_field('booking', 283);

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

		<!-- FEATURE IMAGE
	================================================== -->
    	<section class="feature-image" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')" data-type="background" data-speed="4">
		 <div class="gradient">
			<div class="container">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
		</section>
    
    
    <!-- MAIN CONTENT
	================================================== -->
    <div class="container">
	    <section class="row" id="primary">
	    	<aside id="side-menu" class="col-sm-3">
					<?php
						wp_nav_menu( array(
						
						'theme_location'	=>	'rest-menu-1',
						'container'			=>	'nav',
						'container_class'	=>	'list-unstyled',
						'menu_class'		=>	'',
						
						) );
					?>
					<?php
						wp_nav_menu( array(
						
						'theme_location'	=>	'rest-menu-2',
						'container'			=>	'nav',
						'container_class'	=>	'list-unstyled',
						'menu_class'		=>	'',
						
						) );
					?>
		    </aside><!-- side menu -->
	    
		    <div id="content" class="col-sm-6 menu">
		    	<?php the_content(); ?>
		    </div><!-- main content -->
		    
		    <aside id="sidebar-open" class="col-sm-3">
				<?php if( !empty($open) ) : ?>
		    	<div class="widget">
		    		<h3 class="gold">Öppettider</h3>
					<p class="disclaimer"><?php echo $open; ?></p>
				</div><!-- widget -->
				<?php endif; ?>
				
				<?php if( !empty($gaster) ) : ?>
				<div class="widget">
					<h3 class="gold">Gäster</h3>
					<p class="disclaimer"><?php echo $gaster; ?></p>
				</div><!-- widget -->
				<?php endif; ?> 
				
		<?php get_template_part( 'content', 'bookinglinks' ); ?>	

				

		    </aside><!-- sidbar open -->
	    </section><!-- primary -->
    </div><!-- container -->

<?php endwhile; ?>

<?php
get_footer();
