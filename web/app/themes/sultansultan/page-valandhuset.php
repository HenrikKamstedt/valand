<?php
/*
	Template Name: Valandhuset
 */
// ACF
$ingress		= get_field('ingress');
$open			= get_field('open');
$gaster			= get_field('gaster');
$booking		= get_field('booking');

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

		<!-- FEATURE IMAGE
	================================================== -->
    	<section class="feature-image" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')" data-type="background" data-speed="4">
		 <div class="gradient">
			<div class="container">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
		</section>
    
    
    <!-- MAIN CONTENT
	================================================== -->
    <div class="container">
	    <section class="row" id="primary">
	    	<aside id="side-menu" class="col-sm-3">
					<?php
						wp_nav_menu( array(
						
						'theme_location'	=>	'huset-menu',
						'container'			=>	'nav',
						'container_class'	=>	'list-unstyled',
						'menu_class'		=>	'',
						
						) );
					?>
		    </aside><!-- side menu -->
	    
		    <div id="content" class="col-sm-6">
		    	<p class="lead"><?php the_field('ingress'); ?></p>
		    	<p><?php the_content(); ?></p>
		    </div><!-- main content -->
		    
		    <?php get_template_part( 'content', 'contact' ); ?>

		    
	    </section><!-- primary -->
    </div><!-- container -->

<?php endwhile; ?>

<?php
get_footer();
