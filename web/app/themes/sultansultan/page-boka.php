<?php
/**
	Template Name: Boka bord
 */

// ACF
$post_id 		= 283;
$open			= get_field('open', 283);
$gaster			= get_field('gaster', 283);
$booking		= get_field('booking', 283);

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

		<!-- FEATURE IMAGE
	================================================== -->
    	<section class="feature-image" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')" data-type="background" data-speed="4">
		 <div class="gradient">
			<div class="container">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
		</section>
    
    
    <!-- MAIN CONTENT
	================================================== -->
    <div class="container">
	    <section class="row" id="primary">
	    	<div id="content" class="col-sm-6">
		    	<h2><?php the_field('form1-head'); ?></h2>
		    	<p class="lead"><?php the_field('form1-lead'); ?></p>
		    	<?php the_field('form1-form'); ?>	    
		    </div><!-- content -->
  			
  			<div id="content" class="col-sm-6">
		    	<h2><?php the_field('form2-head'); ?></h2>
		    	<p class="lead"><?php the_field('form2-lead'); ?></p>
		    	<?php the_field('form2-form'); ?>	    
		    </div><!-- content -->
  		
		   
		</section><!-- primary -->
    </div><!-- container -->

<?php endwhile; ?>

<?php
get_footer();
