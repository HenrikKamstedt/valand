<?php
/*
	Template Name: Nattklubb
 */
// ACF
$ingress		= get_field('ingress');
$open			= get_field('open');
$gaster			= get_field('gaster');
$booking		= get_field('booking');

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

		<!-- FEATURE IMAGE
	================================================== -->
    	<section class="feature-image" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')" data-type="background" data-speed="4">
		 <div class="gradient">
			<div class="container">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
		</section>
    
    
    <!-- MAIN CONTENT
	================================================== -->
    <div class="container">
	    <section class="row" id="primary">
	    	<aside id="side-menu" class="col-sm-3">
					<?php
						wp_nav_menu( array(
						
						'theme_location'	=>	'nattklubb-menu',
						'container'			=>	'nav',
						'container_class'	=>	'list-unstyled',
						'menu_class'		=>	'',
						
						) );
					?>
		    </aside><!-- side menu -->
	    
		    <div id="content" class="col-sm-6">
		    	<p class="lead"><?php the_field('ingress'); ?></p>
		    	<p><?php the_content(); ?></p>
		    </div><!-- main content -->
		    
		    <aside id="sidebar-open" class="col-sm-3">
				
				<?php if( !empty($open) ) : ?>
				<div class="widget">
					<h3 class="gold">Öppet</h3>
					<p class="disclaimer"><?php the_field('open'); ?></p>
				</div><!-- widget -->
				<?php endif; ?> 
				
				<div class="widget">
					<a href="http://www.valand-vip.se/" target="_blank" class="btn btn-danger" style="width: 60%;">Gästlista</a>
				</div><!-- widget -->
				
				<div class="widget">
					<a href="<?php the_permalink(368); ?>" class="btn btn-success" style="width: 60%;">Boka drinkbord</a>
				</div><!-- widget -->
				

		    </aside><!-- sidbar open -->
	    </section><!-- primary -->

    </div><!-- container -->

<?php endwhile; ?>

<?php
get_footer();
