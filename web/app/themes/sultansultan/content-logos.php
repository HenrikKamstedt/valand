 <?php
// ACF
$logo1		= get_field('logo1', 86);
$logo2		= get_field('logo2', 86);
$logo3		= get_field('logo3', 86);
$logo4		= get_field('logo4', 86);

?>

	<!-- FOOTER LOGOS
	================================================== -->
	<section id="footer-logos">
		<div class="container">
			<div class="row">
				<div class="col-sm-2 col-xs-6">
          <a href="<?php the_field('logo1-link', 86); ?>">
          <img src="<?php echo $logo1['url']; ?>" alt="<?php echo $logo1['alt']; ?>">
          </a>
				</div><!-- end col -->

				<div class="col-sm-2 col-xs-6">
          <a href="<?php the_field('logo2-link', 86); ?>">
          <img src="<?php echo $logo2['url']; ?>" alt="<?php echo $logo2['alt']; ?>">
          </a>
				</div><!-- end col -->

				<div class="col-sm-2 col-xs-6">
          <a href="<?php the_field('logo3-link', 86); ?>">
					<img src="<?php echo $logo3['url']; ?>" alt="<?php echo $logo3['alt']; ?>">
          </a>
				</div><!-- end col -->

				<div class="col-sm-2 col-xs-6">
					<img src="<?php echo $logo4['url']; ?>" alt="<?php echo $logo4['alt']; ?>">
				</div><!-- end col -->

				<div class="col-sm-4 col-xs-12 form">
					<div class="prenumerera">

<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="//valand.us11.list-manage.com/subscribe/post?u=3d4313351353dc95c5bab439a&amp;id=6b406745be" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">

<div class="mc-field-group">
	<input type="email" value="" name="EMAIL" class="form-control form-footer input required email" id="mce-EMAIL" placeholder="Din e-postadress">
	<input type="submit" value="Prenumerera" name="subscribe" id="mc-embedded-subscribe" class="btn btn-success"></div>
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_3d4313351353dc95c5bab439a_6b406745be" tabindex="-1" value=""></div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text'; }(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
	    			</div>




				</div><!-- end col -->
			</div><!-- row -->
		</div><!-- container -->
	</section>
