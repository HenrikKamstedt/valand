<?php
/*
	Template Name: Introsida Restaurang
 */

// ACF
$ingress		= get_field('ingress');

get_header(); ?>

<!-- FEATURE IMAGE
	================================================== -->
	<section class="feature-image" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')" data-type="background" data-speed="4">
	</section>


    <!-- MAIN CONTENT
	================================================== -->
    <div class="container">
	    <section class="row" id="primary">
	    	<?php $loop = new WP_Query( array(
					'post_type'			=>	'valrestauranger',
          'cat'           => '38',
					'orderby'			=>	'post_id',
					'order'				=>	'ASC',
					) ) ?>
				<?php while( $loop->have_posts() ) : $loop->the_post(); ?>

				<div class="col-sm-12 col-md-4 intro">
					    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        				<?php the_post_thumbnail( 'full' ); ?>
    					</a>
					<h2 class="gold text-center"><?php the_title(); ?></h2>
					<p class="lead intro-fixed text-center"><?php the_field('ingress'); ?></p>
					<p class="text-center"><a href="<?php the_permalink() ?>">L&aumls mer om <?php the_title(); ?></a></p>
	    		</div>
				<?php endwhile; ?>
	    </section><!-- primary -->
    </div><!-- container -->


<?php
get_footer();
