<?php
/**
 * The template for displaying all single event posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Sultan_&_Sultan
 */

// ACF
$event_short	= get_field('event_short');
$event_lead		= get_field('event_lead');
$event_text		= get_field('event_text');
$event_time		= get_field('event_time');
$event_info		= get_field('event_info');
$event_price	= get_field('event_price');
$event_buy		= get_field('event_buy');
$eventdate		= get_field('eventdate');
$eventstart		= get_field('eventstart');
$event_header_color		= get_field('event_header_color');
$facebook_pixel		= get_field('facebook_pixel');

get_header(); ?>

<?php if( !empty($facebook_pixel) ) : ?>
<?php the_field('facebook_pixel'); ?>
<?php endif; ?>

<?php while ( have_posts() ) : the_post(); ?>

		<!-- FEATURE IMAGE
	================================================== -->
	<section class="feature-image" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')" data-type="background" data-speed="4">
		<div class="event-head-left event-head">
			<div class="container">
				<h1><?php the_title(); ?></h1>
				<h3 <?php if( !empty($event_header_color) ) : ?>style="color:<?php the_field('event_header_color'); ?> !important"<?php endif; ?>><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ''; } ?>, <?php if( !empty($eventstart) ) : ?><?php the_field('eventstart'); ?><?php endif; ?> <?php the_field('eventdate'); ?></h3>
				<p <?php if( !empty($event_header_color) ) : ?>style="color:<?php the_field('event_header_color'); ?> !important"<?php endif; ?>><?php the_field('event_short'); ?></p>
			</div>
		</div>
	</section>


    <!-- MAIN CONTENT
	================================================== -->
    <div class="container">
	    <section class="row" id="primary">
	    	<aside id="event-left" class="col-sm-3">
				<div class="widget">
					<h3 class="gold">Detta händer på Valand</h3>
					<ul class="list-unstyled">
				<?php
					$today = date('Ymd');

					$args = array (
    					'post_type' 	=> 'valevent',
    					'meta_key'		=> 'eventdate',
    					'orderby' 		=> 'meta_value',
						'order' 		=> 'ASC',
						'posts_per_page' => 5,
    					'meta_query' 	=> array(
	        				'key'		=> 'eventdate',
	        				'compare'	=> '>=',
	        				'value'		=> $today,
						),
					);

// get posts
$loop = new WP_Query( $args );
?>
				<?php while( $loop->have_posts() ) : $loop->the_post(); ?>
						<li><a href="<?php the_permalink() ?>"><strong><?php the_title(); ?></strong><br>
							<?php if( !empty($eventstart) ) : ?><?php the_field('eventstart'); ?>
							<?php endif; ?>
							<?php the_field('eventdate'); ?></a></li>
				<?php endwhile; wp_reset_query(); ?>
					</ul>


				</div><!-- widget -->
				<div class="widget">
					<p><a class="bokning" href="<?php the_permalink(16) ?>">Se alla kommande event</a></p>
				</div><!-- widget -->
		    </aside><!-- side menu -->

		    <div id="content" class="col-sm-6">
		    	<p class="lead"><?php the_field('event_lead'); ?></p>
		    	<p><?php the_field('event_text'); ?></p>
		    </div><!-- main content -->

		    <aside id="sidebar-open" class="col-sm-3">
		    	<div class="widget">
		    		<h3 class="gold">Datum</h3>
					<p class="disclaimer"><?php if( !empty($eventstart) ) : ?><?php the_field('eventstart'); ?><?php endif; ?><?php the_field('eventdate'); ?></p>
				</div><!-- widget -->

				<?php if( !empty($event_time) ) : ?>
				<div class="widget">
					<h3 class="gold">Tid</h3>
					<p class="disclaimer"><?php the_field('event_time'); ?></p>
				</div><!-- widget -->
				<?php endif; ?>

				<?php if( !empty($event_info) ) : ?>
				<div class="widget">
					<h3 class="gold">Mer info</h3>
					<p class="disclaimer"><?php the_field('event_info'); ?></p>
				</div><!-- widget -->
				<?php endif; ?>

				<?php if( !empty($event_price) ) : ?>
				<div class="widget">
					<h3 class="gold">Pris</h3>
					<p class="disclaimer"><?php the_field('event_price'); ?></p>
				</div><!-- widget -->
				<?php endif; ?>

				<?php if( !empty($event_buy) ) : ?>
                <div class="widget">
					<p><a class="bokning" href="<?php echo $event_buy; ?>" target="_blank">Köp Biljett</a></p>
				</div><!-- widget -->
				<?php endif; ?>

		    </aside><!-- sidbar open -->

	    </section><!-- primary -->
    </div><!-- container -->

<?php endwhile; ?>

<?php
get_footer();
