<?php
/*
	Template Name: Footer
 */
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sultan_&_Sultan
 */

?>

<?php wp_footer(); ?>

<footer>
	<?php get_template_part( 'content', 'categories' ); ?>


	<!-- FOOTER MENU
	================================================== -->
	<section id="footer-menu">
		<div class="container">
			<div class="row">
			<div class="col-md-2 col-sm-3 col-xs-6 foot-col">
					<?php
						wp_nav_menu( array(

						'theme_location'	=>	'fot-menu-1',
						'container'			=>	'nav',
						'container_class'	=>	'',
						'menu_class'		=>	'',

						) );
					?>

			</div><!-- end col -->
			<div class="col-md-2 col-sm-3 col-xs-6 foot-col">
					<?php
						wp_nav_menu( array(

						'theme_location'	=>	'fot-menu-2',
						'container'			=>	'nav',
						'container_class'	=>	'',
						'menu_class'		=>	'',

						) );
					?>
			</div><!-- end col -->
			<div class="col-md-2 col-sm-3 col-xs-6 foot-col">
					<?php
						wp_nav_menu( array(

						'theme_location'	=>	'fot-menu-3',
						'container'			=>	'nav',
						'container_class'	=>	'',
						'menu_class'		=>	'',

						) );
					?>
			</div><!-- end col -->
			<div class="col-md-2 col-sm-3 col-xs-6 foot-col">
					<?php
						wp_nav_menu( array(

						'theme_location'	=>	'fot-menu-4',
						'container'			=>	'nav',
						'container_class'	=>	'',
						'menu_class'		=>	'',

						) );
					?>
			</div><!-- end col -->
			<div class="col-md-2 col-sm-3 col-xs-6 foot-col">
					<?php
						wp_nav_menu( array(

						'theme_location'	=>	'fot-menu-5',
						'container'			=>	'nav',
						'container_class'	=>	'',
						'menu_class'		=>	'',

						) );
					?>
			</div><!-- end col -->

			<div class="col-md-2 col-sm-3 col-xs-6 foot-col">
						<ul class="list-unstyled align-right">
						<li class="parent">VALAND</li>
						<li>Vasagatan 41</li>
						<li>411 36 Göteborg</li>
						<li><a href="callto:031183093">Tel 031-18 30 93 </a></li>
						<li><a href="mailto:info@valand.se">info@valand.se</a></li>
					</ul>
			</div><!-- end col -->
			</div><!-- row -->
			<hr>
		</div><!-- container -->

		<?php get_template_part( 'content', 'logos' ); ?>

	</footer>


   <!-- Bootstrap core JavaScript + other
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/main.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqmx82TIbXxOOM7F0f8GoW21fzaBZxqqc"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/map.js" type="text/javascript"></script>

</body>
</html>
