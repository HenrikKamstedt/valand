<?php
/*
	Template Name: Kalender Kommande
 */

get_header(); ?>


    <!-- DETTA HÄNDER PÅ VALAND
	================================================== -->
	<section id="filter">
		<div class="container">
			<a class="filter-event" href="../kommande-event/">Kommande</a>
			<a class="filter-event" href="../tidigare-event/">Tidigare</a>
			<a class="see-all" href="../alla-event/ ">Se alla event</a>
		</div>
	</section>
	<section id="event-all">
		<div class="container">
			<div class="row">

				<?php
					$today = date('Ymd');

					$args = array (
    					'post_type' 	=> 'valevent',
    					'meta_key'		=> 'eventdate',
    					'orderby' 		=> 'meta_value',
						'order' 		=> 'ASC',
						'posts_per_page' => -1,
    					'meta_query' 	=> array(
	        				'key'		=> 'eventdate',
	        				'compare'	=> '>=',
	        				'value'		=> $today,
						),
					);

// get posts
$loop = new WP_Query( $args );
?>
				<?php while( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="col-sm-4">
					<div class="event event-hover" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>')">
					<a href="<?php the_permalink() ?>" class="event-content">
					<h4><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></h4>
					<h3><?php the_title(); ?></h3>
					<h4>
            <?php if( get_field('eventstart') ): ?><?php the_field('eventstart'); ?>
            <?php endif; ?>
            <?php the_field('eventdate'); ?>
          </h4>
					</a><!-- event-content -->
					</div><!-- event -->
				</div>
				<?php endwhile; wp_reset_query(); ?>

			</div><!-- event row -->
		</div><!-- container -->
	</section><!-- event loop-->

<?php
get_footer();
