<?php
namespace Deployer;

include_once __DIR__ . '/vendor/autoload.php';
include_once __DIR__ . '/vendor/deployer/deployer/recipe/composer.php';

host('staging')
    ->hostname('jizo.oderland.com')
    ->port(22)
    ->set('deploy_path', '/home/sultande/public_html/valand')
    ->user('sultande')
    ->set('branch', 'master')
    ->stage('staging')
    ->identityFile('/Users/Henrik/Webb/nycklar/oderland/id_rsa');

set('repository', 'git@bitbucket.org:HenrikKamstedt/valand.git');

set('env_vars', '/usr/bin/env');
set('keep_releases', 5);
set('shared_dirs', ['web/app/uploads']);
set('shared_files', ['.env']);
